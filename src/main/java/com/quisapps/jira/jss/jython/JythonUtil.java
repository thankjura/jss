package com.quisapps.jira.jss.jython;

import java.io.File;

import org.apache.log4j.Logger;
import org.python.core.Py;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.quisapps.jira.jss.JssUtils;

public class JythonUtil {
	public static final String __INIT_INTERPRETER___PY = "__init_interpreter__.py";
	public static final String JYTHON_VERSION = "2.5.2";
	
	private static Logger log = Logger.getLogger(JythonUtil.class);
	
	private static PythonInterpreter piInstance;
	
	private static boolean reuseInterpreter = 
			Boolean.parseBoolean(System.getProperty("com.quisapps.jira.jss.reuse_int", "false"));
	
	/**
	 * 
	 * @return true if PythonInterpreter instance will be reused across multiple invocations of getInterpreter()
	 *         false if a new instance of PythonInterpreter will be created for each invocation of getInterpreter()
	 */
	public static boolean isReuseInterpreter() {
		return reuseInterpreter;
	}
	
	public static PythonInterpreter getInterpreter() {
		if (reuseInterpreter) {
			if (piInstance == null) {
				piInstance = getNewInterpreter();
			}
			
			return piInstance;
		}
		
		return getNewInterpreter();
	}
	
	public static void resetInterpreter() {
		piInstance = null;
	}
	
	public static PythonInterpreter getNewInterpreter() {
		long millis = System.currentTimeMillis();
		
		try {
			PythonInterpreter pi = new PythonInterpreter(null, null, true) {
				@Override
				public PyObject getLocals() {
					if (threadLocals == null)
						return super.getLocals();
					
					PyObject locals = threadLocals.get();
					if (locals == null) {
						log.debug("Creating new locals for thread " + Thread.currentThread());
						locals = Py.newStringMap();
						threadLocals.set(locals);
					}
					
					return locals;
				}
			};
			pi.execfile(new File(getJythonPath(), __INIT_INTERPRETER___PY).getAbsolutePath());
			
			return pi;
		} finally {
			if (log.isDebugEnabled()) {
				log.debug("new PythonInterpreter() took " + (System.currentTimeMillis() - millis));
			}
		}
	}
	
	/**
	 * 
	 * @return JSS Jython files path
	 */
	public static String getJythonPath() {
		final JiraHome jiraHome = ComponentAccessor.getComponent(JiraHome.class);
		String path = "";
		try {
			File f = new File(new File(jiraHome.getHome(), "jss"), "jython");
			if (!f.exists()) {
				f.mkdirs();
			}
			path = f.getAbsolutePath();
		} catch (final IllegalStateException e) {
			log.fatal(e);
		}
		return path;
	}
	
	/**
	 * 
	 * @return Jython installation dir
	 */
	public static File getJythonHome() {
		return new File(JssUtils.getJssHome(), "jython_" + JYTHON_VERSION);
	}
	
	public static String getInstallerName() {
		return "jython_installer-" + JYTHON_VERSION + ".jar";
	}
}

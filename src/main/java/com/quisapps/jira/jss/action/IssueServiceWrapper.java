package com.quisapps.jira.jss.action;

import org.apache.log4j.Logger;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueInputParameters;
import com.quisapps.jira.jss.ScriptingManager;

public class IssueServiceWrapper extends IssueServiceWrapperBase {
	private static Logger log = Logger.getLogger(IssueServiceWrapper.class);
	
	private ScriptingManager scriptingManager;

	public IssueServiceWrapper(IssueService issueService) {
		super(issueService);
		this.scriptingManager = ComponentAccessor.getOSGiComponentInstanceOfType(ScriptingManager.class);
	}
	
	// UPDATE
	
	@Override
	public UpdateValidationResult validateUpdate(User user,	Long issueId, IssueInputParameters issueInputParameters) {
		UpdateValidationResult validateUpdate = issueService.validateUpdate(user, issueId, issueInputParameters);
		
		try {
			scriptingManager.validateUpdate(validateUpdate.getErrorCollection(), validateUpdate.getIssue());
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
		}
		
		return validateUpdate;
	}
	
	@Override
	public IssueResult update(User user, UpdateValidationResult updateValidationResult) {
	    try {
            scriptingManager.executeUpdatePostfunction(updateValidationResult.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
	    
	    return issueService.update(user, updateValidationResult);
	}
	
	// CREATE
	
	@Override
	public CreateValidationResult validateCreate(User user,
			IssueInputParameters issueInputParameters) {
		CreateValidationResult result = issueService.validateCreate(user, issueInputParameters);
		
		try {
			scriptingManager.validateUpdate(result.getErrorCollection(), result.getIssue());
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
		}
		
		return result;
	}
	
	@Override
	public CreateValidationResult validateSubTaskCreate(User user,
			Long parentId, IssueInputParameters issueInputParameters) {
		CreateValidationResult result = issueService.validateSubTaskCreate(
				user, parentId, issueInputParameters);
		
		try {
            scriptingManager.validateUpdate(result.getErrorCollection(), result.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
		
		return result;
	}
	
	@Override
    public IssueResult create(User user, CreateValidationResult createValidationResult) {
        try {
            scriptingManager.executeUpdatePostfunction(createValidationResult.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return issueService.create(user, createValidationResult);
    }
}

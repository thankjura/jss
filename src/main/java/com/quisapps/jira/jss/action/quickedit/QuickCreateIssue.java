package com.quisapps.jira.jss.action.quickedit;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.TemporaryAttachmentsMonitorLocator;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.rest.FieldHtmlFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.quickedit.user.UserPreferencesStore;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.web.action.issue.IssueCreationHelperBean;
import com.quisapps.jira.jss.action.IssueServiceWrapper;

@SuppressWarnings("serial")
public class QuickCreateIssue extends com.atlassian.jira.quickedit.action.QuickCreateIssue {

	public QuickCreateIssue(IssueFactory issueFactory, IssueCreationHelperBean issueCreationHelperBean, IssueService issueService, UserPreferencesStore userPreferencesStore, UserProjectHistoryManager userProjectHistoryManager, ApplicationProperties applicationProperties, PermissionManager permissionManager, IssueTypeSchemeManager issueTypeSchemeManager, UserIssueHistoryManager userIssueHistoryManager, SubTaskManager subTaskManager, FieldHtmlFactory fieldHtmlFactory, BeanBuilderFactory beanBuilderFactory, JiraBaseUrls jiraBaseUrls, TemporaryAttachmentsMonitorLocator temporaryAttachmentsMonitorLocator) {
        super(issueFactory, issueCreationHelperBean, new IssueServiceWrapper(issueService),
                userPreferencesStore, userProjectHistoryManager, applicationProperties,
                permissionManager, issueTypeSchemeManager, userIssueHistoryManager,
                subTaskManager, fieldHtmlFactory, beanBuilderFactory, jiraBaseUrls, temporaryAttachmentsMonitorLocator);
    }
}

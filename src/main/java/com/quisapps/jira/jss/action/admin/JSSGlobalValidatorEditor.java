package com.quisapps.jira.jss.action.admin;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.PermissionManager;
import com.quisapps.jira.jss.ScriptingManager;
import com.quisapps.jira.jss.install.Installer;

public class JSSGlobalValidatorEditor extends JSSGlobalScriptEditorBase {

	public JSSGlobalValidatorEditor(PermissionManager permissionManager,
			ApplicationProperties applicationProperties,
			ScriptingManager scriptingManager, Installer installer) {
		super(permissionManager, applicationProperties, scriptingManager, installer);
	}
	
	@Override
	public String getSourceTypeProperty() {
		return "com.quisapps.jss.global.edit.validator.source.type";
	}

	@Override
	public String getSourceProperty() {
		return "com.quisapps.jss.global.edit.validator";
	}

	@Override
	public String getDefaultFileName() {
		return "edit_validator.py";
	}

}

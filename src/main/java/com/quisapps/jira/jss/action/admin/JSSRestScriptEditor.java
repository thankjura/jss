package com.quisapps.jira.jss.action.admin;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.PermissionManager;
import com.quisapps.jira.jss.ScriptingManager;

public class JSSRestScriptEditor extends JSSSysScriptEditor {
	
	public JSSRestScriptEditor(PermissionManager permissionManager,
			ApplicationProperties applicationProperties,
			ScriptingManager scriptingManager) {
		super(permissionManager, applicationProperties, scriptingManager);
	}
	
	@Override
	public String getSourceTypeProperty() {
		return "com.quisapps.jss.global.edit.rest.source.type";
	}

	@Override
	public String getSourceProperty() {
		return "com.quisapps.jss.global.edit.rest";
	}

	@Override
	public String getDefaultFileName() {
		return "rest.py";
	}

}

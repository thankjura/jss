package com.quisapps.jira.jss.action.nav;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.components.issueviewer.service.ActionUtilsService;
import com.atlassian.jira.components.issueviewer.service.SessionSearchService;
import com.atlassian.jira.components.issueviewer.viewissue.IssueFieldProvider;
import com.atlassian.jira.components.issueviewer.viewissue.IssueOperationLinksProvider;
import com.atlassian.jira.components.issueviewer.viewissue.IssueSummaryProvider;
import com.atlassian.jira.components.issueviewer.viewissue.webpanel.WebPanelMapperUtil;
import com.atlassian.jira.issue.fields.rest.IssueFinder;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.web.action.issue.IssueMetadataHelper;
import com.quisapps.jira.jss.action.IssueServiceWrapper;

@SuppressWarnings("serial")
public class AjaxIssueAction extends com.atlassian.jira.components.issueviewer.action.AjaxIssueAction {
    //
    public AjaxIssueAction(EventPublisher eventPublisher, IssueFinder issueFinder, IssueMetadataHelper issueMetadataHelper, IssueService issueService, SessionSearchService sessionSearchService, UserIssueHistoryManager userIssueHistoryManager, WebPanelMapperUtil webPanelMapperUtil, ActionUtilsService actionUtilsService, IssueFieldProvider issueFieldProvider, IssueOperationLinksProvider issueOperationLinksProvider, IssueSummaryProvider issueSummaryProvider) {
        super(eventPublisher, issueFinder, issueMetadataHelper, new IssueServiceWrapper(issueService), sessionSearchService, userIssueHistoryManager, webPanelMapperUtil, actionUtilsService, issueFieldProvider, issueOperationLinksProvider, issueSummaryProvider);
    }
}

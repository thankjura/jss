package com.quisapps.jira.jss.action;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.workflow.TransitionOptions;

import java.util.Map;

abstract class IssueServiceWrapperBase implements IssueService {
	protected IssueService issueService;

	IssueServiceWrapperBase(IssueService issueService) {
		this.issueService = issueService;
	}

	@Override
	public IssueResult getIssue(User user, Long issueId) {
		return issueService.getIssue(user, issueId);
	}

	@Override
	public IssueResult getIssue(User user, String issueKey) {
		return issueService.getIssue(user, issueKey);
	}
	
	@Override
	public IssueResult update(User user,
			UpdateValidationResult updateValidationResult,
			EventDispatchOption eventDispatchOption, boolean sendMail) {
		return issueService.update(user, updateValidationResult,
				eventDispatchOption, sendMail);
	}
	
	@Override
	public IssueResult create(User user,
			CreateValidationResult createValidationResult,
			String initialWorkflowActionName) {
		return issueService.create(user, createValidationResult,
				initialWorkflowActionName);
	}
	
	@Override
	public DeleteValidationResult validateDelete(User user, Long issueId) {
		return issueService.validateDelete(user, issueId);
	}

	@Override
	public ErrorCollection delete(User user,
			DeleteValidationResult deleteValidationResult) {
		return issueService.delete(user, deleteValidationResult);
	}

	@Override
	public ErrorCollection delete(User user,
			DeleteValidationResult deleteValidationResult,
			EventDispatchOption eventDispatchOption, boolean sendMail) {
		return issueService.delete(user, deleteValidationResult,
				eventDispatchOption, sendMail);
	}

	@Override
	public boolean isEditable(Issue issue, User user) {
		return issueService.isEditable(issue, user);
	}

	@Override
	public TransitionValidationResult validateTransition(User user,
			Long issueId, int actionId,
			IssueInputParameters issueInputParameters) {
		return issueService.validateTransition(user, issueId, actionId,
				issueInputParameters);
	}

	@Override
	public IssueResult transition(User user,
			TransitionValidationResult transitionResult) {
		return issueService.transition(user, transitionResult);
	}

	@Override
	public AssignValidationResult validateAssign(User user,
			Long issueId, String assignee) {
		return issueService.validateAssign(user, issueId, assignee);
	}

	@Override
	public IssueResult assign(User user,
			AssignValidationResult assignResult) {
		return issueService.assign(user, assignResult);
	}

	@Override
	public IssueInputParameters newIssueInputParameters() {
		return issueService.newIssueInputParameters();
	}

	@Override
	public IssueInputParameters newIssueInputParameters(
			Map<String, String[]> actionParameters) {
		return issueService.newIssueInputParameters(actionParameters);
	}
	
    @Override
    public TransitionValidationResult validateTransition(User user, Long issueId, int actionId,
            IssueInputParameters issueInputParameters, TransitionOptions transitionOptions) {
        return issueService.validateTransition(user, issueId, actionId, issueInputParameters, transitionOptions);
    }
}
package com.quisapps.jira.jss.action.admin;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.PermissionManager;
import com.quisapps.jira.jss.ScriptingManager;
import com.quisapps.jira.jss.install.Installer;

public abstract class JSSGlobalScriptEditorBase extends JSSSysScriptEditor {

	private Installer installer;

	public JSSGlobalScriptEditorBase(PermissionManager permissionManager,
			ApplicationProperties applicationProperties,
			ScriptingManager scriptingManager, Installer installer) {
		super(permissionManager, applicationProperties, scriptingManager);
		
		this.installer = installer;
	}

	@Override
	protected String doExecute() throws Exception {
		if (!installer.isQuickEditEnabled()) {
			addErrorMessage("Global Edit Validation & Postfunction scripts are disabled due" +
					" to installation problem: incompatible Quick Edit plugin.");
		}
		
		return super.doExecute();
	}

}
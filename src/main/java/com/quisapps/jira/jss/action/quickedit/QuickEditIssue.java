package com.quisapps.jira.jss.action.quickedit;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.fields.rest.FieldHtmlFactory;
import com.atlassian.jira.quickedit.user.UserPreferencesStore;
import com.atlassian.jira.quickedit.user.UserPreferencesStoreImpl;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.user.UserPropertyManager;
import com.quisapps.jira.jss.action.IssueServiceWrapper;

@SuppressWarnings("serial")
public class QuickEditIssue extends com.atlassian.jira.quickedit.action.QuickEditIssue {
	public QuickEditIssue(IssueService issueService, UserPreferencesStore userPreferencesStore, UserIssueHistoryManager userIssueHistoryManager, FieldHtmlFactory fieldHtmlFactory) {
        super(new IssueServiceWrapper(issueService), userPreferencesStore, userIssueHistoryManager, fieldHtmlFactory);
    }
}

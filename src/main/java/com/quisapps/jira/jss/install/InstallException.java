package com.quisapps.jira.jss.install;

public class InstallException extends Exception {
	private static final long serialVersionUID = -5752538578098116468L;

	public InstallException() {
		super();
	}

	public InstallException(String message, Throwable cause) {
		super(message, cause);
	}

	public InstallException(String message) {
		super(message);
	}

	public InstallException(Throwable cause) {
		super(cause);
	}
}
package com.quisapps.jira.jss.install;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.quisapps.jira.jss.jython.JythonUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("install")
public class InstallerRestService {
	
	private Installer installer;
	private JiraAuthenticationContext authenticationContext;
	private GlobalPermissionManager globalPermissionManager;
	
	public InstallerRestService(Installer installer,
			JiraAuthenticationContext authenticationContext,
			GlobalPermissionManager globalPermissionManager) {
		this.installer = installer;
		this.authenticationContext = authenticationContext;
		this.globalPermissionManager = globalPermissionManager;
	}

	@GET
	@Path("status")
	@Produces(MediaType.APPLICATION_JSON)
	@AnonymousAllowed
	public Response getInstallStatus() {
		if (!isAuthorized()) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		JSONObject obj = new JSONObject();
		try {
			obj.put("scriptsInstalled", installer.isBaseScriptsInstalled());
			obj.put("scriptsPath", JythonUtil.getJythonPath());
			obj.put("jythonInstalled", installer.isJythonInstalled());
			obj.put("jythonPath", JythonUtil.getJythonHome().getAbsolutePath());
			obj.put("jythonVersion", JythonUtil.JYTHON_VERSION);
			obj.put("pythonHome", System.getProperty("python.home"));
			obj.put("quickedit", installer.isQuickEditEnabled());
			obj.put("issuenav", installer.isIssueNavEnabled());
			
			
		} catch (JSONException e) {
			throw new WebApplicationException(e, Status.INTERNAL_SERVER_ERROR);
		}
		
		return Response.ok(obj.toString()).build();
	}
	
	@GET
	@Path("reinstall")
	@AnonymousAllowed
	public Response reinstall() {
		if (!isAuthorized()) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		installer.reinstall();
		
		return Response.ok().build();
	}
	
	private boolean isAuthorized() {
		ApplicationUser user = authenticationContext.getUser();
		if (user == null) {
			return false;
		}
		
		boolean isAdmin = globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
		return isAdmin;
	}
}

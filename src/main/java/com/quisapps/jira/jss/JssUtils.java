package com.quisapps.jira.jss;

import java.io.File;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;

public class JssUtils {

	public static File getJssHome() {
		final JiraHome jiraHome = ComponentAccessor.getComponent(JiraHome.class);
		File jssHome = new File(jiraHome.getHome(), "jss");
		if (!jssHome.exists()) {
			jssHome.mkdirs();
		}
		return jssHome;
	}

}

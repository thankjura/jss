package com.quisapps.jira.jss;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.action.issue.AbstractIssueSelectAction;
import com.atlassian.jira.web.bean.PagerFilter;
import com.quisapps.jira.jss.action.admin.JSSGlobalPostfunctionEditor;
import com.quisapps.jira.jss.action.admin.JSSGlobalScriptEditorBase;
import com.quisapps.jira.jss.action.admin.JSSGlobalValidatorEditor;
import com.quisapps.jira.jss.action.admin.JSSSysScriptEditor;
import com.quisapps.jira.jss.install.Installer;
import com.quisapps.jira.jss.jython.JythonContext;
import com.quisapps.jira.jss.jython.JythonUtil;
import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.python.core.*;
import org.python.util.PythonInterpreter;
import webwork.action.ActionContext;
import webwork.action.factory.ActionFactory;

import java.util.Map;

public class ScriptingManagerImpl implements ScriptingManager {
	private static Logger log = Logger.getLogger(ScriptingManagerImpl.class);
    public com.atlassian.jira.web.bean.PagerFilter pagerFilter;
	
	private Installer installer;
	private ApplicationProperties applicationProperties;

	public ScriptingManagerImpl(Installer installer,
			ApplicationProperties applicationProperties) {
		this.installer = installer;
		this.applicationProperties = applicationProperties;
        this.pagerFilter = PagerFilter.getUnlimitedFilter();
	}
	
	@Override
	public JythonContext getJythonContext() {
		return new JythonContext();
	}
	
	@Override
	@Deprecated
	public void doEditValidation(AbstractIssueSelectAction action) {
		validateUpdate(action, action.getIssueObject());
	}
	
	@Override
	@Deprecated
	public void doEditPostfunction(AbstractIssueSelectAction action) {
		executeUpdatePostfunction(action.getIssueObject());
	}
	
	@Override
	public void executeUpdatePostfunction(MutableIssue issueObject) {
		if (!installer.isInstalled()) {
			log.error("JSS installation is not completed");
			return;
		}
		
		try {
			PythonInterpreter pi = JythonUtil.getInterpreter();
			PyObject locals_save = pi.getLocals();
			
			try {
				if (JythonUtil.isReuseInterpreter()) {
					pi.setLocals(Py.newStringMap());
				}
			
				pi.set(JythonContext.LOG, log);
				pi.set(JythonContext.ISSUE, issueObject);
				
				JSSGlobalScriptEditorBase vAction;
				try {
					vAction = (JSSGlobalScriptEditorBase)
						ActionFactory.getAction(JSSGlobalPostfunctionEditor.class.getSimpleName());
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					return;
				}
				
				String jythonSourceType = applicationProperties.getString(vAction.getSourceTypeProperty());
				if (JSSSysScriptEditor.JYTHON_SOURCE_TYPE_INLINE.equals(jythonSourceType)) {
					pi.exec(new PyUnicode(applicationProperties.getText(vAction.getSourceProperty())));
				} else {
					pi.execfile(vAction.getJythonSourceFile().getAbsolutePath() );
				}
			} finally {
				if (JythonUtil.isReuseInterpreter()) {
					pi.setLocals(locals_save);
				}
			}
		} catch (Throwable ex) {
			log.error(ex);
		}
	}
	
	@Override
	public boolean validateUpdate(ErrorCollection errors, MutableIssue issueObject) {
		if (!installer.isInstalled()) {
			log.error("JSS installation is not completed");
			return true;
		}
		
		PythonInterpreter pi = JythonUtil.getInterpreter();
		PyObject locals_save = pi.getLocals();
		
		try {
			if (JythonUtil.isReuseInterpreter()) {
				pi.setLocals(Py.newStringMap());
			}
		
			pi.set(JythonContext.LOG, log);
			pi.set(JythonContext.RESULT, new Boolean(true));
			pi.set(JythonContext.DESCRIPTION, new PyUnicode());
			pi.set(JythonContext.ISSUE, issueObject);
			pi.set("parameters", ActionContext.getParameters());
			pi.set(JythonContext.INVALID_FIELDS, new PyDictionary());
			
			JSSGlobalValidatorEditor vAction;
			try {
				vAction = (JSSGlobalValidatorEditor)
					ActionFactory.getAction(JSSGlobalValidatorEditor.class.getSimpleName());
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return true;
			}
			
			String jythonSourceType = applicationProperties.getString(vAction.getSourceTypeProperty());
			if (JSSSysScriptEditor.JYTHON_SOURCE_TYPE_INLINE.equals(jythonSourceType)) {
				pi.exec(new PyUnicode(applicationProperties.getText(vAction.getSourceProperty())));
			} else {
				pi.execfile(vAction.getJythonSourceFile().getAbsolutePath());
			}
			
			boolean result = ((Boolean) pi.get(JythonContext.RESULT).__tojava__(Boolean.class)).booleanValue();
			if (result) {
				return true;
			}
			
			String description = "";
			Object descObj = pi.get(JythonContext.DESCRIPTION);
			if (descObj instanceof PyUnicode) {
				description = ((PyUnicode) descObj).asString();
			} else {
				description = descObj.toString();
			}
			errors.addErrorMessage(description);
			
			if (pi.get(JythonContext.INVALID_FIELDS) != null)
			{
				@SuppressWarnings("rawtypes")
				Map invF = (PyDictionary)pi.get(JythonContext.INVALID_FIELDS);
				for (Object field : invF.keySet()) {
					Object fieldDesc = invF.get(field);
					errors.addError(field.toString(),
							fieldDesc instanceof PyUnicode ?
								((PyUnicode) fieldDesc).asString() :
								fieldDesc.toString()
					);
				}
			}
			
			return false;
		} finally {
			if (JythonUtil.isReuseInterpreter()) {
				pi.setLocals(locals_save);
			}
		}
	}

    @Override
    public void executeScript(String script, Map<String, Object> context) {
        if (!installer.isInstalled()) {
            log.error("JSS installation is not completed");
            return;
        }
        
        PythonInterpreter pi = JythonUtil.getInterpreter();
        PyObject locals_save = pi.getLocals();
        
        try {
            if (JythonUtil.isReuseInterpreter()) {
                pi.setLocals(Py.newStringMap());
            }
            
            for (String ctxKey : context.keySet()) {
                pi.set(ctxKey, context.get(ctxKey));
            }
            
            pi.exec(new PyUnicode(script));
            
            context.put("__locals__", pi.getLocals());
            
            PyStringMap locals = (PyStringMap) pi.getLocals();
            for (Object key : locals.keys()) {
                String sKey = ObjectUtils.toString(key);
                PyObject value = locals.__finditem__(sKey);
                context.put(sKey, value.__tojava__(Object.class));
            }
        } finally {
            if (JythonUtil.isReuseInterpreter()) {
                pi.setLocals(locals_save);
            }
        }
    }
}

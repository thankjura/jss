package com.quisapps.jira.jss;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.quisapps.jira.jss.install.Installer;

public class PluginInitializer implements InitializingBean {
	private static Logger log = Logger.getLogger(PluginInitializer.class);
	
	private EventPublisher eventPublisher;
	private Installer installer;
	private PluginController pluginController;
	
	public PluginInitializer(EventPublisher eventPublisher,
			Installer installer, PluginController pluginController) {
		this.eventPublisher = eventPublisher;
		this.installer = installer;
		this.pluginController = pluginController;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		eventPublisher.register(this);
	}
	
	@EventListener
	public void onEvent(PluginEnabledEvent event) {
		Plugin plugin = event.getPlugin();
		
		if (!plugin.getKey().equals("com.quisapps.jira.jss")) {
			return;
		}
		
		Logger root = Logger.getLogger("com.quisapps.jira");
		root.setLevel(Level.INFO);
		
		try {
			// install Jython and base scripts
			installer.install();
			
			ModuleDescriptor<?> qe;
			
			qe = plugin.getModuleDescriptor("jss.actions.quickedit");
			try {
				// exception will be thrown if patched version of quickedit plugin
				// is not installed (with Export-Package: com.atlassian.jira.quickedit.*)
				String nameCreate = com.atlassian.jira.quickedit.action.QuickCreateIssue.class.getName();
				String nameEdit = com.atlassian.jira.quickedit.action.QuickEditIssue.class.getName();
				
				pluginController.enablePluginModule(qe.getCompleteKey());
				installer.setQuickEditEnabled(true);
				
				log.info("Enabled QuickEdit support: " + nameCreate + "; " + nameEdit);
			} catch (Throwable t) {
				// we must disable our custom quickedit handler to keep this function working
				pluginController.disablePluginModule(qe.getCompleteKey());
				installer.setQuickEditEnabled(false);
				
				log.info("Disabled QuickEdit support: " + t.getMessage());
			}
			
			qe = plugin.getModuleDescriptor("jss.actions.issuenav");
            try {
                // exception will be thrown if patched version of issuenav plugin
                // is not installed (with Export-Package: com.atlassian.jira.plugin.issuenav.*)
                String name = com.atlassian.jira.components.issueviewer.action.AjaxIssueAction.class.getName();
                
                pluginController.enablePluginModule(qe.getCompleteKey());
                installer.setIssueNavEnabled(true);
                
                log.info("Enabled IssueNav support: " + name);
            } catch (Throwable t) {
                // we must disable our custom issuenav handler to keep this function working
                pluginController.disablePluginModule(qe.getCompleteKey());
                installer.setIssueNavEnabled(false);
                
                log.info("Disabled IssueNav support: " + t.getMessage());
            }
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			eventPublisher.unregister(this);
		}
	}
}

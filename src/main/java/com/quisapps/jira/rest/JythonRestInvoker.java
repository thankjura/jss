package com.quisapps.jira.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.python.core.Py;
import org.python.core.PyObject;
import org.python.core.PyUnicode;
import org.python.util.PythonInterpreter;

import webwork.action.factory.ActionFactory;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.quisapps.jira.jss.action.admin.JSSRestScriptEditor;
import com.quisapps.jira.jss.action.admin.JSSSysScriptEditor;
import com.quisapps.jira.jss.install.Installer;
import com.quisapps.jira.jss.jython.JythonContext;
import com.quisapps.jira.jss.jython.JythonUtil;

@Path("/jython/invoke")
public class JythonRestInvoker {
	private static final Logger log = Logger.getLogger(JythonRestInvoker.class);
	
	private ApplicationProperties applicationProperties;
	private Installer installer;
	
	public JythonRestInvoker(ApplicationProperties applicationProperties, Installer installer) {
		this.applicationProperties = applicationProperties;
		this.installer = installer;
	}
	
	@GET
	@Path("{method}")
	@AnonymousAllowed
	@Produces(MediaType.APPLICATION_JSON)
	public Response invokeGet(@PathParam("method") String method) {
		try {
			log.debug("GET: " + method + "()");
			return run(method, "{}");
		} catch (Throwable t) {
			log.error(t);
			
			return Response.serverError().entity(
					new ExceptionBean(t)).build();
		}
	}
	
	@POST
	@Path("{method}")
	@AnonymousAllowed
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public Response invokePost(@PathParam("method") String method, String args) {
		try {
			log.debug("POST: " + method + "('" + args + "')");
			
			return run(method, args);
		} catch (Throwable ex) {
			log.error(ex);
			
			return Response.serverError().entity(
					new ExceptionBean(ex)).build();
		}
	}
	
	@GET
	@Path("{module}/{method}")
	@AnonymousAllowed
	@Produces(MediaType.APPLICATION_JSON)
	public Response invokeGet(
			@PathParam("module") String module,
			@PathParam("method") String method) {
		
		try {
			log.debug(String.format(
					"GET: module=%s; method=%s", module, method));
			return run(module, method, "{}");
		} catch (Throwable t) {
			log.error(t);
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(
					new ExceptionBean(t)).build();
		}
	}
	
	@POST
	@Path("{module}/{method}")
	@AnonymousAllowed
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public Response invokePost(
			@PathParam("module") String module,
			@PathParam("method") String method,
			String args ) {
		
		try {
			log.debug(String.format(
					"POST: module=%s; method=%s; args=%s",
					module, method, args));
			
			return run(module, method, args);
		} catch (Throwable ex) {
			log.error(ex);
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(
					new ExceptionBean(ex)).build();
		}
	}
	
	private Response run(String module, String method, String args) {
		if (!installer.isInstalled()) {
			log.error("JSS installation is not completed");
			return Response.notModified().build();
		}
		
		PythonInterpreter pi = JythonUtil.getInterpreter();
		PyObject locals_save = pi.getLocals();
		
		try {
			if (JythonUtil.isReuseInterpreter()) {
				pi.setLocals(Py.newStringMap());
			}
			
			pi.set(JythonContext.LOG, log);
			pi.set("module", module);
			pi.set("method", method);
			pi.set("args", args);
			
			JSSRestScriptEditor vAction;
			try {
				vAction = (JSSRestScriptEditor)
					ActionFactory.getAction(JSSRestScriptEditor.class.getSimpleName());
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return Response.serverError().build();
			}
			
			String jythonSourceType = applicationProperties.getString(vAction.getSourceTypeProperty());
			if (JSSSysScriptEditor.JYTHON_SOURCE_TYPE_INLINE.equals(jythonSourceType)) {
				pi.exec(new PyUnicode(applicationProperties.getText(vAction.getSourceProperty())));
			} else {
				pi.execfile(vAction.getJythonSourceFile().getAbsolutePath() );
			}
			
			String result = pi.get(JythonContext.RESULT).toString();
			return Response.ok(result).build();
		} finally {
			if (JythonUtil.isReuseInterpreter()) {
				pi.setLocals(locals_save);
			}
		}
	}
	
	private Response run(String method, String args) {
		return run(null, method, args);
	}
}

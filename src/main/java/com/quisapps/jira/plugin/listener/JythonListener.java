package com.quisapps.jira.plugin.listener;

import java.io.File;
import java.util.Map;

import org.apache.log4j.Logger;
import org.python.core.Py;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventListener;
import com.atlassian.jira.event.user.UserEvent;
import com.atlassian.jira.event.user.UserEventListener;
import com.quisapps.jira.jss.jython.JythonContext;
import com.quisapps.jira.jss.jython.JythonUtil;
import com.quisapps.jira.plugin.workflow.JythonFileUtil;

public class JythonListener implements IssueEventListener, UserEventListener {
    private static final Logger log = Logger.getLogger(JythonListener.class);

    private String getFullPath(String fileName) {
    	return new File(new File(JythonUtil.getJythonPath(), "listener"), fileName).getAbsolutePath();
    }
    
	@Override
	public void customEvent(IssueEvent event) {
		log.info(event);
	}

	@Override
	public void userCreated(UserEvent event) {
	}

	@Override
	public void userForgotPassword(UserEvent event) {
	}

	@Override
	public void userForgotUsername(UserEvent event) {
	}

	@Override
	public void userSignup(UserEvent event) {
	}
	
	@Override
	public void userCannotChangePassword(UserEvent arg0) {
	}
	
	@Override
	public void workflowEvent(IssueEvent event) {
		try {
			PythonInterpreter pi = JythonUtil.getInterpreter();
			PyObject locals_save = pi.getLocals();
			
			try {
				if (JythonUtil.isReuseInterpreter()) {
					pi.setLocals(Py.newStringMap());
				}
				
				pi.set(JythonContext.LOG, log);
				pi.set(JythonContext.EVENT, event);
				
				pi.execfile(getFullPath("workflow_event.py"));
			} finally {
				if (JythonUtil.isReuseInterpreter()) {
					pi.setLocals(locals_save);
				}
			}
		} catch (Throwable ex) {
			log.error(ex);
		}
	}
	
	@Override
	public void issueAssigned(IssueEvent event) {}
	@Override
	public void issueClosed(IssueEvent event) {}
	@Override
	public void issueCommented(IssueEvent event) {}
	@Override
	public void issueCreated(IssueEvent event) {}
	@Override
	public void issueDeleted(IssueEvent event) {}
	@Override
	public void issueGenericEvent(IssueEvent event) {}
	@Override
	public void issueMoved(IssueEvent event) {}
	@Override
	public void issueReopened(IssueEvent event) {}
	@Override
	public void issueResolved(IssueEvent event) {}
	@Override
	public void issueStarted(IssueEvent event) {}
	@Override
	public void issueStopped(IssueEvent event) {}
	@Override
	public void issueUpdated(IssueEvent event) {}
	@Override
	public void issueWorkLogged(IssueEvent event) {}

	@Override
	public String[] getAcceptedParams() {
		return new String[0];
	}

	@Override
	public String getDescription() {
		return "Jython Listener";
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void init(Map params) {
		File fld = new File(JythonFileUtil.getInstance().getJythonPath(), "listener");
		if (!fld.exists()) {
			fld.mkdirs();
		} else if (!fld.isDirectory()) {
			log.error(fld + " supposed to be a directory, not a file!");
		}
	}

	@Override
	public boolean isInternal() {
		return false;
	}

	@Override
	public boolean isUnique() {
		return true;
	}
}

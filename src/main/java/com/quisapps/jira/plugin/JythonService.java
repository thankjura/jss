package com.quisapps.jira.plugin;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.apache.log4j.Logger;
import org.python.core.Py;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.service.AbstractService;
import com.opensymphony.module.propertyset.PropertySet;
import com.quisapps.jira.jss.jython.JythonUtil;

public class JythonService extends AbstractService {
	private static Logger log = Logger.getLogger(JythonService.class);
	
	private static final String KEY_FILE_NAME = "FILE_NAME";
	private static final String KEY_LOG_NAME = "LOG_NAME";
	public String fileName;
	public String logName;
	
	@Override
	public void run() {
		PythonInterpreter pi = JythonUtil.getInterpreter();
		PyObject locals_save = pi.getLocals();
		
		try {
			if (JythonUtil.isReuseInterpreter()) {
				pi.setLocals(Py.newStringMap());
			}
				
			if (logName != null) {
				try {
					pi.setOut(new FileOutputStream(logName));
				} catch (FileNotFoundException e) {
					log.warn("Can't create or open " + logName + "; using stdout", e);
				}
			}
			pi.execfile(fileName);
		} finally {
			if (JythonUtil.isReuseInterpreter()) {
				pi.setLocals(locals_save);
			}
		}
	}
	
	@Override
	public void init(PropertySet props) throws ObjectConfigurationException {
		super.init(props);
		
		fileName = getProperty(JythonService.KEY_FILE_NAME);
		logName = getProperty(JythonService.KEY_LOG_NAME);
		log.info("JythonService.init : " + KEY_FILE_NAME + "=" + fileName + 
				"; " + KEY_LOG_NAME + "=" + logName);
	}
	
	@Override
	public ObjectConfiguration getObjectConfiguration()
			throws ObjectConfigurationException {
		return getObjectConfiguration(
				"JYTHONSERVICE",
				"jythonservice.xml", null);
	}

}

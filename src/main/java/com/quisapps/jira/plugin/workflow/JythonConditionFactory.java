package com.quisapps.jira.plugin.workflow;

import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;

@SuppressWarnings("unchecked")
public class JythonConditionFactory
	extends AbstractJythonWorkflowPluginFactory
	implements WorkflowPluginConditionFactory {
	
	{
		MODULE_NAME = "quisapps.jython.condition";
	}
	
}

package com.quisapps.jira.plugin.workflow;

import java.io.File;
import java.util.Map;

import org.apache.log4j.Logger;
import org.python.core.Py;
import org.python.core.PyDictionary;
import org.python.core.PyObject;
import org.python.core.PyUnicode;
import org.python.util.PythonInterpreter;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.quisapps.jira.jss.jython.JythonContext;
import com.quisapps.jira.jss.jython.JythonUtil;

public class JythonValidator implements Validator {
	private static Logger log = Logger.getLogger(JythonValidator.class);
	
	@Override
	public void validate(@SuppressWarnings("rawtypes") Map transientVars,
			@SuppressWarnings("rawtypes") Map args, PropertySet arg2)
			throws InvalidInputException, WorkflowException
	{
		try {
			PythonInterpreter pi = JythonUtil.getInterpreter();
			PyObject locals_save = pi.getLocals();
			
			try {
				if (JythonUtil.isReuseInterpreter()) {
					pi.setLocals(Py.newStringMap());
				}
				
				pi.set(JythonContext.LOG, log);
				pi.set(JythonContext.RESULT, true);
				pi.set(JythonContext.DESCRIPTION, new PyUnicode());
				pi.set(JythonContext.ISSUE, transientVars.get("issue"));
				pi.set(JythonContext.ORIGINALISSUE, transientVars.get("originalissueobject"));
				pi.set(JythonContext.TRANSIENT_VARS, transientVars);
				pi.set(JythonContext.INVALID_FIELDS, new PyDictionary());
				
				String condition = ((String)args.get(
						AbstractJythonWorkflowPluginFactory.SCRIPT_FIELD_NAME)).trim();
				if (condition.startsWith("@")) {
					pi.execfile(new File(JythonFileUtil.getInstance().getJythonPath(),
							condition.substring(1)).getAbsolutePath() );
				} else {
					pi.exec(new PyUnicode(condition));
				}
				
				boolean result = ((Boolean) pi.get(JythonContext.RESULT).__tojava__(Boolean.class)).booleanValue();
				if (!result) {
					String description = "";
					Object descObj = pi.get(JythonContext.DESCRIPTION);
					if (descObj instanceof PyUnicode)
						description = ((PyUnicode) descObj).asString();
					else 
						description = descObj.toString();
					
					InvalidInputException ex = new InvalidInputException(description);
					if (pi.get(JythonContext.INVALID_FIELDS) != null)
					{
						@SuppressWarnings("rawtypes")
						Map invF = (PyDictionary)pi.get(JythonContext.INVALID_FIELDS);
						for (Object field : invF.keySet()) {
							Object fieldDesc = invF.get(field);
							ex.addError(field.toString(),
									fieldDesc instanceof PyUnicode ?
										((PyUnicode) fieldDesc).asString() :
										fieldDesc.toString()
								);
						}
					}
					
					throw ex;
				}
			} finally {
				if (JythonUtil.isReuseInterpreter()) {
					pi.setLocals(locals_save);
				}
			}
		} catch (InvalidInputException ex) {
			throw ex;
		} catch (Exception ex) {
			log.error("Unrecognized exception while executing Jyphon script: " + ex.getMessage(), ex);
			throw new WorkflowException(ex);
		}
	}
}

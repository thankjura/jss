package com.quisapps.jira.plugin.workflow;

import java.io.File;
import java.util.Map;

import org.apache.log4j.Logger;
import org.python.core.Py;
import org.python.core.PyObject;
import org.python.core.PyUnicode;
import org.python.util.PythonInterpreter;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.quisapps.jira.jss.jython.JythonContext;
import com.quisapps.jira.jss.jython.JythonUtil;

public class JythonPostFunction implements FunctionProvider {
	private static Logger log = Logger.getLogger(JythonPostFunction.class);
	
	@Override
	public void execute(@SuppressWarnings("rawtypes") Map transientVars,
			@SuppressWarnings("rawtypes") Map args, PropertySet arg2) throws WorkflowException
	{
		try {
			PythonInterpreter pi = JythonUtil.getInterpreter();
			PyObject locals_save = pi.getLocals();
			
			try {
				if (JythonUtil.isReuseInterpreter()) {
					pi.setLocals(Py.newStringMap());
				}
				
				pi.set(JythonContext.LOG, log);
				pi.set(JythonContext.ISSUE, transientVars.get("issue"));
				pi.set(JythonContext.TRANSIENT_VARS, transientVars);
				
				String condition = ((String)args.get(
						AbstractJythonWorkflowPluginFactory.SCRIPT_FIELD_NAME)).trim();
				if (condition.startsWith("@")) {
					pi.execfile(new File(JythonFileUtil.getInstance().getJythonPath(),
							condition.substring(1)).getAbsolutePath() );
				} else {
					pi.exec(new PyUnicode(condition));
				}
			} finally {
				if (JythonUtil.isReuseInterpreter()) {
					pi.setLocals(locals_save);
				}
			}
		} catch (Exception ex) {
			log.error("Unrecognized exception while executing Jyphon script: " + ex.getMessage(), ex);
			throw new WorkflowException(ex);
		}
	}
}

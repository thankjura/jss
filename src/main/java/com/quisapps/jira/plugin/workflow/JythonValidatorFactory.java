package com.quisapps.jira.plugin.workflow;

import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;

@SuppressWarnings("unchecked")
public class JythonValidatorFactory extends
		AbstractJythonWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
	{
		MODULE_NAME = "quisapps.jython.validator";
	}
}

package com.quisapps.jira.plugin.workflow;

import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;

@SuppressWarnings("unchecked")
public class JythonPostFunctionFactory extends AbstractJythonWorkflowPluginFactory
		implements WorkflowPluginFunctionFactory {

	{
		MODULE_NAME = "quisapps.jython.postfunction";
	}
	
}

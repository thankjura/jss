package ru.otr.jira.jython;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import org.python.core.PyDictionary;
import org.python.core.PyList;
import org.python.core.PyObject;
import org.python.core.PyString;

import java.util.Iterator;

/**
 * Created by jura on 16.12.14.
 */
public class JsonCodec {
    private static PyList getList(JSONArray json) throws JSONException {
        PyList out = new PyList();
        for (int i=0; i< json.length(); i++) {
            Object v = json.get(i);
            if (v instanceof JSONObject) {
                out.add(getDict((JSONObject) v));
                continue;
            }
            if (v instanceof JSONArray) {
                out.add(getList((JSONArray) v));
                continue;
            }
            out.add(getString(v));
        }
        return out;
    }

    private static PyString getString(Object json) {
        return new PyString((String) json);
    }

    private static PyDictionary getDict(JSONObject json) throws JSONException {
        PyDictionary out = new PyDictionary();
        Iterator<?> keys = json.keys();
        while (keys.hasNext()) {
            String key = (String)keys.next();
            Object v = json.get(key);
            if (v instanceof JSONObject) {
                out.put(key, getDict((JSONObject)v));
                continue;
            }
            if (v instanceof JSONArray) {
                out.put(key, getList((JSONArray)v));
                continue;
            }
            out.put(key, getString(v));
        }
        return out;
    }

    public static PyObject loads(PyString json) throws JSONException {
        String jsonText = json.toString();
        switch (jsonText.charAt(0)) {
            case '[':
                return getList(new JSONArray(jsonText));
            case '{':
                return getDict(new JSONObject(jsonText));
            case '"':
                return getString(jsonText);
        }

        throw new JSONException("Can't loads");
    }

    public static JSONArray outList(PyList list) throws JSONException {
        JSONArray out = new JSONArray();
        for (int i=0; i< list.size(); i++) {
            Object v = list.get(i);
            if (v instanceof PyDictionary) {
                out.put(outDict((PyDictionary)v));
                continue;
            }
            if (v instanceof JSONArray) {
                out.put(outList((PyList) v));
                continue;
            }
            out.put(v.toString());
        }
        return out;
    }

    public static JSONObject outDict(PyDictionary dict) throws JSONException {
        JSONObject out = new JSONObject();
        for (Object key: dict.keySet()) {
            Object val = dict.get(key);
            if (val instanceof PyDictionary) {
                out.append((String)key, outDict((PyDictionary)val));
                continue;
            }
            if (val instanceof PyList) {
                out.append((String)key, outList((PyList)val));
                continue;
            }
            out.put((String)key, val.toString());
            //out.append((String)key, val.toString());
        }
        return out;
    }

    public static PyString dumps(PyObject json) throws JSONException {
        PyString x;
        if (json instanceof PyDictionary) {
            x= new PyString(outDict((PyDictionary)json).toString());
        } else if (json instanceof PyList) {
            x= new PyString(outList((PyList) json).toString());
        } else {
            x= new PyString(json.toString());
        }
        return x;
    }
}

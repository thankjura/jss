var jythonTextFieldChanged = false;
var jythonTextFieldName = "jythonSource";
var jythonFileSelectPrevValues = null;

function jythonInit(fieldName)  {
	jythonTextFieldName = fieldName; 
	
	jythonFileSelectPrevValues = jQuery('#jythonFileSelect').val();

	var buttons = {
		'Yes': function() {
			jQuery(this).dialog('close');
			loadJython(jQuery('#jythonFileSelect').val());
		},
		'No': function() {
			jQuery(this).dialog('close');
		},
		'Cancel': function() { 
			jQuery('#jythonFileSelect').val(jythonFileSelectPrevValues);
			jQuery(this).dialog('close');
		}
	};
	if (jQuery.ui.version == "1.7.2") {
		buttons = {
				'Cancel' : buttons['Cancel'],
				'No' : buttons['No'],
				'Yes' : buttons['Yes']
		}
	}
	
	jQuery('#dialogDiscard').dialog({
		modal: true,
		autoOpen: false,
		width: 350,
		buttons: buttons,
		close: function() { jythonFileSelectPrevValues = jQuery('#jythonFileSelect').val(); }
	});
	
	jQuery('#dialogWait').dialog({
		modal: true,
		draggable: false,
		title: 'Please wait...',
		resizable: false,
		autoOpen: false,
		width: 246,
		height: 32,
		minHeight: 20
	}).css('height', '23px');
	jQuery('div:has(#dialogWait) div:first').hide();
	
	jQuery('#jythonFileSelect').change(function() {
		jQuery('#jythonNewFileName').attr('disabled', (this.value != '-1'));
		if (this.value != '-1') {
			if (jythonTextFieldChanged) {
				jQuery('#dialogDiscard').dialog('open');
			} else {
				jythonFileSelectPrevValues = jQuery('#jythonFileSelect').val();
				loadJython(this.value);
			}
		}
	});
	jQuery('#jythonSourceTypeInline').click(function() {
		jQuery('#jythonFileSelect,#jythonNewFileName').attr('disabled', true);
	});
	jQuery('#jythonSourceTypeFile').click(function() { 
		jQuery('#jythonFileSelect').attr('disabled', false).trigger('change');
	});
	
	jQuery('#jythonDefaults').hide();
	jQuery('#jythonDefaultsToggle').click(function() { jQuery('#jythonDefaults').slideToggle('slow') });
	
	editAreaLoader.init({
		id : jythonTextFieldName	// textarea id
		,syntax: "python"			// syntax to be uses for highgliting
		,start_highlight: true		// to display with highlight mode on start-up
		,change_callback: "valChanged"
	});

	var text = jQuery('#'+jythonTextFieldName).val(); 
	if (text.charAt(0) == '@') {
		jQuery('#jythonFileSelect option[value="' + text.substring(1) + '"]').attr('selected', 1);
		jQuery('#jythonSourceTypeFile').attr('checked', true);
		jQuery('#jythonFileSelect').attr('disabled', false).trigger('change');
	}	
}

function loadJython(file) {
	jQuery('#dialogWait').attr('showTimeoutId', 
			setTimeout("jQuery('#dialogWait').dialog('open');", 500)
	);
	jQuery.ajax({
		url: contextPath + '/rest/jss/1.0/jython/files/' + file,
		type: 'GET',
		success: function(data) {
			clearTimeout(jQuery('#dialogWait').attr('showTimeoutId'));
			if (jQuery("#dialogWait").dialog('isOpen')) {
				jQuery("#dialogWait").dialog('close');
			}
			
			editAreaLoader.setValue(jythonTextFieldName, data);
			editAreaLoader.setSelectionRange(jythonTextFieldName, 0, 0);
			jythonTextFieldChanged = false;
		},
		complete: function() {
			clearTimeout(jQuery('#dialogWait').attr('showTimeoutId'));
			if (jQuery("#dialogWait").dialog('isOpen')) {
				jQuery("#dialogWait").dialog('close');
			}
		}
	});
}

function valChanged(id) {
	jythonTextFieldChanged = true;
}


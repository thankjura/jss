# -*- coding: UTF-8 -*-
import sys

if sys.getdefaultencoding() != 'utf-8':
  from org.python.core import codecs
  codecs.setDefaultEncoding('utf-8')

from com.quisapps.jira.plugin.workflow import JythonFileUtil
path = JythonFileUtil.getInstance().getJythonPath()
if path not in sys.path:
  sys.path.append(path)

from com.quisapps.jira.jss.jython import JythonUtil
path = JythonUtil.getJythonPath()
if path not in sys.path:
  sys.path.append(path)

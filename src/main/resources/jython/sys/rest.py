# -*- coding: UTF-8 -*-
from com.atlassian.jira import ComponentManager
import sys

# SECURITY WARNING!
# All functions are publicly available by default,
# i.e. any unauthenticated user can call them.
# You are self-responsible for checking user permissions.
# See test(args) below for usage of checkUser().
def checkUser():
	authenticationContext = ComponentManager.getInstance().getJiraAuthenticationContext()
	user = authenticationContext.getUser()

	if not user:
		raise Exception("JYTHON REST: Invalid User Context")

	return user

# URL: <baseurl>/rest/jss/1.0/jython/invoke/<function>
# 	or <baseurl>/rest/jss/1.0/jython/invoke/<module>/<function>
# this function URL is ${baseurl}/rest/jss/1.0/jython/invoke/test
def test(args):
	# this will return valid User object or throw a message in case
	# of not authenticated attempt 
	user = checkUser()
	
	print 'test!', args
	return {"name" : "val"}

# return value of a function should always be a map!
# this should be the last lines in the file
from ru.otr.jira.jython import JsonCodec as json
try:
	if module:
		__import__("rest", fromlist = [module])
		mdl = sys.modules["rest.%s" % module]
		reload(mdl)
		result = json.dumps(getattr(mdl, method)(json.loads(args)))
	else:
		result = json.dumps(locals()[method](json.loads(args)))
except Exception, e:
	log.warn(e.__str__())
	result = json.dumps({'error': e.__str__()})
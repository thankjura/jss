<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
	<plugin-info>
		<description>${project.description}</description>
		<version>${project.version}</version>
		<vendor name="${project.organization.name}" url="${project.organization.url}" />
	</plugin-info>
	
	<component key="ScriptingManager" name="Scripting Manager" public="true" class="com.quisapps.jira.jss.ScriptingManagerImpl">
		<interface>com.quisapps.jira.jss.ScriptingManager</interface>
	</component>

	<rest key="${project.artifactId}.rest" path="/jss" version="1.0">
		<description>Provides JSS REST services</description>
	</rest>
	
	<web-resource key="editarea" name="EditArea" state="enabled">
		<resource type="download" name="edit_area/edit_area_full.js" location="edit_area/edit_area_full.js" >
			<param name="batch" value="false"/>
		</resource>
		<resource type="download" name="edit_area/" location="edit_area/" >
			<param name="batch" value="false"/>
		</resource>
	</web-resource>
	
	<web-resource key="jquery-ui-css" name="" >
		<resource type="download" name="jquery-ui.css" location="css/smoothness/jquery-ui-1.8.6.custom.css" />
		<resource type="download" name="images/" location="css/smoothness/images/" />
	</web-resource>
	
	<web-resource key="jquery-ui-js" name="" >
		<resource type="download" name="jquery-ui.js" location="js/jquery-ui-1.8.6.custom.min.js" />
		<dependency>com.atlassian.auiplugin:ajs</dependency>
	</web-resource>
	
	<web-resource key="jython-js-tools" name="Jython JS Tools" state="enabled">
		<resource type="download" name="js/jython-edit.js" location="js/jython-edit.js" />
		<dependency>com.atlassian.auiplugin:ajs</dependency>
	</web-resource>
	
	<workflow-validator key="quisapps.jython.validator" name="Jython Validator" class="com.quisapps.jira.plugin.workflow.JythonValidatorFactory">
		<description>Executes the specified Jython script and checks the value returned</description>
		<validator-class>com.quisapps.jira.plugin.workflow.JythonValidator</validator-class>
		
		<resource type="velocity" name="view" location="templates/plugin/jython-validator-view.vm" />
		<resource type="velocity" name="input-parameters" location="templates/plugin/jython-validator-edit.vm" />
		<resource type="velocity" name="edit-parameters" location="templates/plugin/jython-validator-edit.vm" />
		<resource type="velocity" name="jython-file-panel" location="templates/plugin/jython-file-panel.vm" />
		<resource type="velocity" name="jython-reference" location="templates/plugin/jython-reference.vm" />
	</workflow-validator>
	
	<workflow-function key="quisapps.jython.postfunction" name="Jython Post Function" class="com.quisapps.jira.plugin.workflow.JythonPostFunctionFactory">
		<description>Executes the specified Jython script</description>
		<function-class>com.quisapps.jira.plugin.workflow.JythonPostFunction</function-class>
		
		<orderable>true</orderable>
		<unique>false</unique>
		<deletable>true</deletable>
		<default>false</default>
		
		<resource type="velocity" name="view" location="templates/plugin/jython-function-view.vm" />
		<resource type="velocity" name="input-parameters" location="templates/plugin/jython-function-edit.vm" />
		<resource type="velocity" name="edit-parameters" location="templates/plugin/jython-function-edit.vm" />
		<resource type="velocity" name="jython-file-panel" location="templates/plugin/jython-file-panel.vm" />
		<resource type="velocity" name="jython-reference" location="templates/plugin/jython-reference.vm" />
	</workflow-function>

	<workflow-condition key="quisapps.jython.condition"
		name="Jython Condition" class="com.quisapps.jira.plugin.workflow.JythonConditionFactory">
		<description>Executes the specified Jython script and checks the value returned</description>
		<condition-class>com.quisapps.jira.plugin.workflow.JythonCondition</condition-class>
		
		<resource type="velocity" name="view" location="templates/plugin/jython-condition-view.vm" />
		<resource type="velocity" name="input-parameters" location="templates/plugin/jython-condition-edit.vm" />
		<resource type="velocity" name="edit-parameters" location="templates/plugin/jython-condition-edit.vm" />
		<resource type="velocity" name="jython-file-panel" location="templates/plugin/jython-file-panel.vm" />
		<resource type="velocity" name="jython-reference" location="templates/plugin/jython-reference.vm" />
	</workflow-condition>
	
	<web-section key="jss" name="JSS Settings" location="admin_plugins_menu" weight="150">
		<description>Quisapps Jira Scripting Suite settings</description>
		<label>Scripting Suite</label>
	</web-section>
	
	<web-item key="jss.edit.validator" name="Global Edit Validation" section="admin_plugins_menu/jss" weight="10">
		<description>Allows to edit Jython script for edit validator</description>
		<label>Global Edit Validation</label>
		<link linkId="jss_edit_validator">/secure/admin/JSSGlobalValidatorEditor.jspa</link>
	</web-item>
	
	<web-item key="jss.edit.postfunction" name="Global Edit Postfunction" section="admin_plugins_menu/jss" weight="11">
		<description>Allows to edit Jython script for edit postfunction</description>
		<label>Global Edit Postfunction</label>
		<link linkId="jss_edit_postfunction">/secure/admin/JSSGlobalPostfunctionEditor.jspa</link>
	</web-item>
	
	<web-item key="jss.rest.editor" name="REST Script" section="admin_plugins_menu/jss" weight="12">
		<description>Jython REST Services Script Editor</description>
		<label>REST Script</label>
		<link linkId="jss_rest">/secure/admin/JSSRestScriptEditor.jspa</link>
	</web-item>
	
	<web-item key="jss.jython.runner" name="Jython Runner" section="admin_plugins_menu/jss" weight="13">
		<description>Jython Script Runner</description>
		<label>Jython Runner</label>
		<link linkId="jss_jython_runner">/secure/admin/JythonRunner!default.jspa</link>
	</web-item>
	
	<web-item key="jss.installer.action" name="JSS Installer" section="admin_plugins_menu/jss" weight="14">
		<description>JSS Installer</description>
		<label>JSS Installer</label>
		<link linkId="jss_installer">/secure/admin/JSSInstaller!default.jspa</link>
	</web-item>
	
	<webwork1 key="jss.actions" name="JSS Actions">
		<actions>
			<action name="com.quisapps.jira.jss.action.admin.JSSGlobalValidatorEditor" alias="JSSGlobalValidatorEditor">
				<view name="default">/templates/plugin/action/syseditor-default.vm</view>
				<view name="input">/templates/plugin/action/global-validator-edit.vm</view>
				<view name="error">/templates/plugin/action/global-validator-edit.vm</view>
			</action>
			<action name="com.quisapps.jira.jss.action.admin.JSSGlobalPostfunctionEditor"
					alias="JSSGlobalPostfunctionEditor">
				<view name="default">/templates/plugin/action/syseditor-default.vm</view>
				<view name="input">/templates/plugin/action/global-postfunction-edit.vm</view>
				<view name="error">/templates/plugin/action/global-postfunction-edit.vm</view>
			</action>
			<action name="com.quisapps.jira.jss.action.admin.JSSRestScriptEditor" alias="JSSRestScriptEditor">
				<view name="default">/templates/plugin/action/syseditor-default.vm</view>
				<view name="input">/templates/plugin/action/rest-script-edit.vm</view>
				<view name="error">/templates/plugin/action/rest-script-edit.vm</view>
			</action>
			<action name="com.quisapps.jira.jss.action.admin.JythonRunnerAction" alias="JythonRunner">
				<view name="default">/templates/plugin/action/jython-runner.vm</view>
				<view name="input">/templates/plugin/action/jython-runner.vm</view>
				<view name="error">/templates/plugin/action/jython-runner.vm</view>
			</action>
			<action name="com.quisapps.jira.jss.action.admin.InstallerAction" alias="JSSInstaller">
				<view name="default">/templates/plugin/action/installer.vm</view>
				<view name="input">/templates/plugin/action/installer.vm</view>
				<view name="error">/templates/plugin/action/installer.vm</view>
			</action>
			<action name="com.quisapps.jira.jss.action.EditIssue" alias="EditIssue">
				<view name="error">/secure/views/issue/editissue.jsp</view>
				<view name="input">/secure/views/issue/editissue.jsp</view>
			</action>
		</actions>
	</webwork1>
	
	<webwork1 key="jss.actions.quickedit" name="JSS Quick Edit Actions">
		<actions>
			<action name="com.quisapps.jira.jss.action.quickedit.QuickEditIssue" alias="QuickEditIssue"></action>
			<action name="com.quisapps.jira.jss.action.quickedit.QuickCreateIssue" alias="QuickCreateIssue"></action>
		</actions>
	</webwork1>
    
    <webwork1 key="jss.actions.issuenav" name="JSS Issue Nav Actions">
        <actions>
			<action name="com.quisapps.jira.jss.action.nav.AjaxIssueAction" alias="AjaxIssueAction"/>
        </actions>
    </webwork1>
	
	<ao key="ao.install.state" name="Installation State Active Object">
		<entity>com.quisapps.jira.jss.ao.InstallState</entity>
	</ao>
	
	<component-import key="ao" name="Active Objects service" interface="com.atlassian.activeobjects.external.ActiveObjects">
		<description>Component to access Active Objects functionality from the plugin</description>
	</component-import>

    <component-import key="searchService" interface="com.atlassian.jira.bc.issue.search.SearchService"></component-import>

	<component key="jss.plugin.init" name="Plugin Initializer" class="com.quisapps.jira.jss.PluginInitializer">
		<interface>com.quisapps.jira.jss.PluginInitializer</interface>
	</component>
	
	<component key="jss.installer" name="JSS Installer" class="com.quisapps.jira.jss.install.InstallerImpl">
		<interface>com.quisapps.jira.jss.install.Installer</interface>
	</component>

	<component key="jython.json" name="json component for jython" class="ru.otr.jira.jython.JsonCodec"></component>
</atlassian-plugin>